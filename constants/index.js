'use strict'

module.exports = {
  MONGO_URL: 'mongodb://localhost:27017/',
  DB_NAME: {
    development: 'busTickets',
    TEST: 'busTicketsTest'
  },
  AUTH_SECRET: 'S3cR3T_k3Y'
}
