process.env.NODE_ENV = 'TEST'

const server = require('../server')

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
chai.use(chaiHttp);

const inputs = require('./inputs')
let authToken = ''

describe('Testing all admin operations', function() {
  it('Register user', function(done) {
    chai.request(server)
      .post('/api/v1/register')
      .send(inputs.createUser)
      .end(function (err, res) {
        console.log('errerr', res.body)
        res.should.have.status(201)
        res.body.should.have.property('_id');
        done()
      })
  })

  it('Login user', function(done) {
    chai.request(server)
      .post('/api/v1/login')
      .send(inputs.loginUser)
      .end(function (err, res) {
        authToken = res.body.token
        res.body.should.have.property('token')
        done()
      })
  })

  it('Initialize should return error without authentication', function(done) {
    chai.request(server)
      .post('/api/v1/initialize')
      .end(function (err, res) {
        res.should.have.status(401)
        res.body.message.should.equal('Not authorized to access this resource')
        done()
      })
  })

  it('Initialize', function(done) {
    chai.request(server)
      .post('/api/v1/initialize')
      .set({ Authorization: authToken })
      .end(function (err, res) {
        res.should.have.status(201)
        res.body.message.should.equal('Tickets created successfullly!')
        done()
      })
  })

  it('Check if 40 tickets are created', function(done) {
    chai.request(server)
      .get('/api/v1/ticketsByStatus/open')
      .end(function (err, res) {
        res.body.should.be.a('array')
        res.body.length.should.equal(40)
        done()
      })
  })

  it('Close one ticket', function(done) {
    chai.request(server)
      .post('/api/v1/tickets/1/setStatus')
      .send(inputs.closeTicket)
      .end(function (err, res) {
        res.body.nModified.should.equal(1)
        done()
      })
  })

  it('Check closed tickets count', function(done) {
    chai.request(server)
      .get('/api/v1/ticketsByStatus/close')
      .end(function (err, res) {
        res.body.should.be.a('array')
        res.body.length.should.equal(1)
        // console.log('Check closed tickets count', err, res.body)
        done()
      })
  })

  it('Reset Tickets should return authentication error', function(done) {
    chai.request(server)
      .post('/api/v1/reset')
      .end(function (err, res) {
        res.should.have.status(401)
        res.body.message.should.equal('Not authorized to access this resource')
        console.log('reset', res.body)
        done()
      })
  })

  it('Reset Tickets:', function(done) {
    chai.request(server)
      .post('/api/v1/reset')
      .set({ Authorization: authToken })
      .end(function (err, res) {
        res.should.have.status(201)
        res.body.message.should.equal('All tickets opened successfullly!')
        done()
      })
  })

  it('Check All tickets are reseted', function(done) {
    chai.request(server)
      .get('/api/v1/ticketsByStatus/open')
      .end(function (err, res) {
        res.body.length.should.equal(40)
        done()
      })
  })
})

describe('Testing all ticket operations', function() {
  it('should list ALL the open tickets', function(done) {
    chai.request(server)
      .get('/api/v1/ticketsByStatus/open')
      .end(function (err, res) {
        res.body.length.should.equal(40)
        // console.log('err, res', err, res.body)
        done()
      })
  })

  it('Close one ticket', function(done) {
    chai.request(server)
      .post('/api/v1/tickets/2/setStatus')
      .send(inputs.closeTicket)
      .end(function (err, res) {
        res.body.nModified.should.equal(1)
        done()
      })
  })

  it('Should not allow to re-book a closed ticket', function(done) {
    chai.request(server)
      .post('/api/v1/tickets/2/setStatus')
      .send(inputs.closeTicket)
      .end(function (err, res) {
        res.should.have.status(400)
        res.body.message.should.equal('Ticket has already been booked by another user')
        // console.log('err, res', err, res.body)
        done()
      })
  })

  it('Should return error ticket not found', function(done) {
    chai.request(server)
      .post('/api/v1/tickets/41/setStatus')
      .send(inputs.closeTicket)
      .end(function (err, res) {
        res.should.have.status(400)
        res.body.message.should.equal('Ticket not found')
        // console.log('ERROR', err, res.body)
        done()
      })
  })

  it('should list ALL the closed tickets', function(done) {
    chai.request(server)
      .get('/api/v1/ticketsByStatus/close')
      .end(function (err, res) {
        res.body.length.should.equal(1)
        // console.log('closed tickets', err, res.body)
        done()
      })
  })

  it('should return status of ticket 2', function(done) {
    chai.request(server)
      .get('/api/v1/tickets/2/status')
      .end(function (err, res) {
        res.body.status.should.equal('close')
        // console.log('status of ticket 2', err, res.body)
        done()
      })
  })

  it('should return error if ticket status not found', function(done) {
    chai.request(server)
      .get('/api/v1/tickets/41/status')
      .end(function (err, res) {
        res.should.have.status(400)
        res.body.message.should.equal('Ticket not found')
        // console.log('ERRORRRR', err, res.body)
        done()
      })
  })

  it('should return details of ticket 2', function(done) {
    chai.request(server)
      .get('/api/v1/tickets/2/details')
      .end(function (err, res) {
        res.body.should.have.property('user')
        // console.log('details of ticket 2', err, res.body)
        done()
      })
  })

  it('Open closed ticket', function(done) {
    chai.request(server)
      .post('/api/v1/tickets/2/setStatus')
      .send(inputs.openTicket)
      .end(function (err, res) {
        res.body.nModified.should.equal(1)
        done()
      })
  })

  it('should return error if ticket details not found', function(done) {
    chai.request(server)
      .get('/api/v1/tickets/41/details')
      .end(function (err, res) {
        res.should.have.status(400)
        res.body.message.should.equal('Ticket not found')
        // console.log('ERRORRRR in details', err, res.body)
        done()
      })
  })
})
