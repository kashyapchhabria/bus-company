const express = require('express');
const bodyParser = require('body-parser');
require('./db')

// create express app
const app = express();

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json())

const v1 = require('./services').v1

app.use('/api/v1', v1)

// listen for requests
app.listen(3000, () => {
  console.log('Server is listening on port 3000');
});

module.exports = app;
