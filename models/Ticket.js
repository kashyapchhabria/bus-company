const mongoose = require('mongoose')
const validator = require('validator')

const userSchema = mongoose.Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  age: {
    type: Number,
    required: true
  },
  gender: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    lowercase: true,
    validate: value => {
      if (!validator.isEmail(value)) {
        throw new Error('Invalid Email address')
      }
    }
  }
})

const ticketSchema = mongoose.Schema({
  seatNo: {
    type: Number,
    required: true
  },
  status: {
    type: String,
    enum: ['open', 'close'],
    required: true
  },
  user: userSchema
})

ticketSchema.statics.updateTicketsByStatus = async function (query, payload, callback) {
  const test = new Ticket(payload)
  const error = test.validateSync()
  if (error) {
    throw error
  }
  const result = await Ticket.updateOne(query, { $set: payload }, callback)
  if (result && result.nModified < 1) {
    throw new Error('Ticket not found')
  }
  return result
}

ticketSchema.statics.findTicketsByStatus = async function (status, callback) {
  const tickets = await Ticket.find({ status: status }, callback)
  return tickets
}

ticketSchema.statics.findBookingStatus = async function (query, callback) {
  const ticket = await Ticket.findOne(query, 'status', callback)
  if (!ticket) {
    throw new Error('Ticket not found')
  }
  return ticket
}

ticketSchema.statics.findBookingDetails = async function (seatNo, callback) {
  const ticket = await Ticket.findOne({ seatNo }, 'user', callback)
  if (!ticket) {
    throw new Error('Ticket not found')
  } else if (!ticket.user) {
    throw new Error('Ticket User Details not available')
  }
  return ticket
}

const Ticket = mongoose.model('Ticket', ticketSchema)

module.exports = Ticket
