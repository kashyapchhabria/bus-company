'use strict'

const express = require('express')
const router = express.Router()

require('./tickets').register(router)
require('./user').register(router)

module.exports = router
