const data = require('../../db/tickets.json')
const Ticket = require('../../models/Ticket')
const auth = require('../../middlewares/auth')

const initialize = async function (req, res) {
  try {
    await Ticket.deleteMany()
    await Ticket.create(data.tickets)
    res.status(201).json({ message: 'Tickets created successfullly!' })
  } catch (error) {
    res.status(400).json({ message: error.message })
  }
}

const reset = async function (req, res) {
  try {
    await Ticket.updateMany({}, { $set: { status: 'open' }, $unset: { user: '' }})
    res.status(201).json({ message: 'All tickets opened successfullly!' })
  } catch (error) {
    res.status(400).json({ message: error.message })
  }
}

const ticketsByStatus = async function (req, res) {
  try {
    const status = req.params.status
    const tickets = await Ticket.findTicketsByStatus(status)
    res.status(201).send(tickets)
  } catch (error) {
    res.status(400).json({ message: error.message })
  }
}

const setStatus = async function (req, res) {
  try {
    const payload = {
      seatNo: Number(req.params.seatNo),
      status: req.body.status
    }
    if (req.body.user && req.body.status === 'close') {
      payload.user = req.body.user
    } else {
      payload.user = null
    }
    const query = { seatNo: req.params.seatNo }
    let ticket = await Ticket.findOne(query)
    if (!ticket) {
      res.status(400).json({ message: 'Ticket not found' })
    } else if (ticket.status === 'close' && payload.status === 'close') {
      res.status(400).json({ message: 'Ticket has already been booked by another user' })
    } else {
      ticket = await Ticket.updateTicketsByStatus(query, payload)
      res.status(201).send(ticket)
    }
  } catch (error) {
    res.status(400).json({ message: error.message })
  }
}

const getStatus = async function (req, res) {
  try {
    const query = { seatNo: req.params.seatNo }
    const ticket = await Ticket.findBookingStatus(query)
    res.status(201).send(ticket)
  } catch (error) {
    res.status(400).json({ message: error.message })
  }
}

const getDetails = async function (req, res) {
  try {
    const ticket = await Ticket.findBookingDetails(req.params.seatNo)
    res.status(201).send(ticket)
  } catch (error) {
    res.status(400).json({ message: error.message })
  }
}

exports.register = (router) => {
  // Initialize Bus Tickets
  // /api/v1/initialize
  router.post('/initialize', auth, initialize)

  // /api/v1/reset
  router.post('/reset', auth, reset)

  // /api/v1/ticketsByStatus/:status
  router.get('/ticketsByStatus/:status', ticketsByStatus)

  // /api /v1/tickets/:seatNo/setStatus
  router.post('/tickets/:seatNo/setStatus', setStatus)

  // /api/v1/tickets/:seatNo/status
  router.get('/tickets/:seatNo/status', getStatus)

  // /api/v1/tickets/:seatNo/details
  router.get('/tickets/:seatNo/details', getDetails)
}
