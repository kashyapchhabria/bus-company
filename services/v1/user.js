const User = require('../../models/User')

const create = async function (req, res) {
  try {
    const user = new User(req.body)
    await user.save()
    res.status(201).send(user)
  } catch (error) {
    res.status(400).send(error)
  }
}

const login = async function (req, res, next) {
  // Login a registered user
  try {
    const { email, password } = req.body
    const user = await User.findByCredentials(email, password)
    if (!user) {
      return res.status(401).send({error: 'Login failed! Check authentication credentials'})
    }
    const token = await user.generateAuthToken()
    res.send({ user, token })
  } catch (error) {
    res.status(401).json({ message: error.message })
  }
}

exports.register = (router) => {
  // /api/v1/register
  router.post('/register', create)

  // /api/v1/login
  router.post('/login', login)
}
