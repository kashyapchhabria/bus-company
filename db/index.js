const mongoose = require('mongoose')
const constants = require('../constants')
const express = require('express')
const app = express()

const env = app.settings.env
console.log('environment => ', env)
mongoose.connect(`${constants.MONGO_URL}${constants.DB_NAME[env]}`, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: true,
  useUnifiedTopology: true
})
