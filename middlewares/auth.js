const jwt = require('jsonwebtoken')
const User = require('../models/User')
const constants = require('../constants')

const auth = async(req, res, next) => {
  if (req.header('Authorization')) {
    const token = req.header('Authorization').replace('Bearer ', '')
    const data = jwt.verify(token, constants.AUTH_SECRET)
    try {
      const user = await User.findOne({ _id: data._id, 'tokens.token': token })
      if (!user) {
        res.status(401).json({ message: 'Not authorized to access this resource'})
      }
      req.user = user
      req.token = token
      next()
    } catch (error) {
      res.status(401).json({ message: 'Not authorized to access this resource'})
    }
  } else {
    res.status(401).json({ message: 'Not authorized to access this resource'})
  }
}
module.exports = auth